var currentPage = 1;

function loadPage(num) {
    currentPage = num;
    $.ajax({
        url: '/api/sinhvien/?page=' + num,
        type: 'GET'
    })
        .then(data => {
            $('#list').html('');
            console.log(data);
            for (let i = 0; i < data.length; i++) {
                const e = data[i];
                var item = $(`<h1>${e.hotenSinhVien}</h1>`);
                $('#list').append(item);
            }

        })
        .catch(err => {
            console.log("API lỗi");
        })
}
function nextPage() {
    currentPage = currentPage + 1;
    $.ajax({
        url: '/api/sinhvien/?page=' + currentPage,
        type: 'GET'
    })
        .then(data => {
            $('#list').html('');
            console.log(data);
            for (let i = 0; i < data.length; i++) {
                const e = data[i];
                var item = $(`<h1>${e.hotenSinhVien}</h1>`);
                $('#list').append(item);
            }

        })
        .catch(err => {
            console.log("API lỗi");
        })
}
function previousPage() {
    currentPage = currentPage - 1;
    $.ajax({
        url: '/api/sinhvien/?page=' + currentPage,
        type: 'GET'
    })
        .then(data => {
            $('#list').html('');
            console.log(data);
            for (let i = 0; i < data.length; i++) {
                const e = data[i];
                var item = $(`<h1>${e.hotenSinhVien}</h1>`);
                $('#list').append(item);
            }

        })
        .catch(err => {
            console.log("API lỗi");
        })
}
loadPage(1);