const express = require('express');
var router = express.Router();


const service = require('../services/sinhvien.service');
const con = require('../db');




router.get('/findsv/:id', async (req, res) => {
    var sv = await service.getSVById(req.params.id);
    if (sv.length == 0) {
        res.status(404).json('Không tìm thấy sinh viên ' + req.params.id);
    }
    res.send(sv);
})

router.delete('/xoasv', async (req, res) => {
    var id = req.body.id;
    console.log(id);
    const sv = await service.deleteSVById(id);
    if (sv == 0) {
        res.status(404).json("Không có sinh vien " + id);
    }
    else {
        res.send("Xoá thành công!");
    }

})
router.put('/suasv', async (req, res) => {
    var id = req.body.id;
    var hoten = req.body.hoten;
    var gioitinh = req.body.gioitinh;
    var ngaysinh = req.body.ngaysinh;
    const sv = await service.updateSinhVien(id, hoten, gioitinh, ngaysinh);
    if (sv == 0) {
        res.status(404).json("Không có sinh vien " + id);
    }
    else {
        res.send("sưa thông tin thành công");
    }
})
router.post('/themsv', async (req, res) => {
    var hoten = req.body.hoten;
    var gioitinh = req.body.gioitinh;
    var ngaysinh = req.body.ngaysinh;
    var diachi = req.body.diachi;
    const sv = await service.themSV(hoten, gioitinh, ngaysinh, diachi);
    console.log(sv);
    if (sv == 0) {
        res.status(404).json("Thêm mới sinh viên không thành công");
    }
    else {
        res.send("Thêm mới thành công!!!");
    }
})
router.get('/countsvlop', async (req, res) => {
    const total = await service.getTotalLop();
    console.log(total);
    if (total == 0) {
        res.status(404).json("Lỗi ở đếm sinh viên theo lớp");
    }
    else {
        res.send(total);
    }
});
router.get('/countsvnganh', async (req, res) => {
    const total = await service.getTotalNganh();
    console.log(total);
    if (total == 0) {
        res.status(404).json("Lỗi ở đếm sinh viên theo ngành");
    }
    else {
        res.send(total);
    }
});
//phân trang dữ liệu sinh viên 
router.get('/', async (req, res) => {
    const PAGE_SIZE = 2;
    var page = req.query.page;

    console.log(page);
    if (page) {
        page = parseInt(page);
        if (page >= 1) {
            var skip = (page - 1) * PAGE_SIZE;//bỏ qua phần tử
            var total = await service.getSVpage(skip, PAGE_SIZE);
            res.send(total[0]);
        }
        else {//viết số âm hoặc chữ thì trả về trang 1
            var total = await service.getSVpage(0, PAGE_SIZE);
            res.send(total[0]);
        }
    }
    else {
        var dssv = await service.getAllSV();
        res.send(dssv);
    }

})





module.exports = router;