const express = require('express');
const app = express();
const con = require('./db');
const bodyParser = require('body-parser');
var svRouter = require('./controllers/sinhvien.controller')
const path = require('path');

app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

app.use('/api/sinhvien/', svRouter);

app.use((err, req, res, next) => {
    console.log(err);
    res.status(err.status || 500).send("có lỗi " + err);

});
app.use('/',express.static('./public'));


app.get('/home',(req,res)=>{
    res.sendFile(path.join(__dirname,'public/index.html'))
})


con.query("select 1")
    .then(() => {
        console.log("kết nối thành công với db!!")
        app.listen(8080, () => console.log("server is running at 8080 "));
    })
    .catch(err => console.log("kết nối thất bại " + err));

