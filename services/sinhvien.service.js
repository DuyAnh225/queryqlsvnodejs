const con = require('../db');

module.exports.getAllSV = async () => {
    const dssv = await con.query("select * from SinhVien")
        .catch(err => console.log(err))
    return dssv;
}

module.exports.getSVById = async (id) => {
    var sv = await con.query(`select * from SinhVien where maSinhVien =${id}`);
    return sv;
}

module.exports.deleteSVById = async (id) => {
    var sv = await con.query(`delete from SinhVien where maSinhVien =${id}`);
    return sv;
}
module.exports.updateSinhVien = async (id, hoten, gioitinh, ngaysinh) => {
    var sv = await con.query(`update SinhVien
    set hoTenSinhVien ='${hoten}',
    gioiTinhSinhVien =${gioitinh},
    ngaySinhSinhVien = '${ngaysinh}'
    where maSinhVien = ${id}`);
    return sv;
}
module.exports.themSV = async (hoten, gioitinh, ngaysinh, diachi) => {
    var sv = await con.query(`insert into SinhVien(hoTenSinhVien,gioiTinhSinhVien, ngaySinhSinhVien, diaChiSinhVien) values
    ('${hoten}',${gioitinh},'${ngaysinh}','${diachi}')`);
    return sv;
}
module.exports.getTotalLop = async () => {
    const total = await con.query(`SELECT  LopHoc.tenLop,  COUNT(*) AS soLuongSinhVien
    FROM  Diem JOIN  LopHoc ON  Diem.maLop = LopHoc.maLop
    GROUP BY  LopHoc.maLop;`);
    return total;
}

module.exports.getTotalNganh = async () => {
    const total = await con.query(`SELECT  tenNganhHoc,  COUNT(*) AS soLuongSinhVien
    FROM  Diem inner join NganhHoc on Diem.maNganhHoc = NganhHoc.maNganhHoc
    GROUP BY  Diem.maNganhHoc;`);
    return total;
}
module.exports.getSVpage = async (page, pageSize) => {
    const total = await con.query(`SELECT * FROM sinhvien
    limit ${page},${pageSize};`)
    return total;
}
